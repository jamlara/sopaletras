/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.movimientos;

/**
 *
 * @author w10
 */
import com.indra.juego.Casilla;
import com.indra.juego.PalabraCasilla;
import java.awt.Point;
import java.util.Random;

public class ColocarHorizontal extends Colocar {

    public ColocarHorizontal(PalabraCasilla palabra) {
        super(palabra);
    }

    @Override
    public void generarPuntosTablero(int filas, int columnas) {
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j <= columnas - getPalabra().getTamaño(); j++) {
                puntosTablero.add(new Point(i, j));
            }
        }
    }

    @Override
    public boolean validarCasilla(Point coordenadaInicio, int posicion) {

        Casilla[][] tableroAsterisco = this.getCasillas();

        boolean esValido = true;

        if (tableroAsterisco[coordenadaInicio.x][coordenadaInicio.y + posicion].getCaracter() != '*') {
            if (tableroAsterisco[coordenadaInicio.x][coordenadaInicio.y + posicion].getCaracter() != getPalabra().getCasillas().get(posicion).getCaracter()) {
                puntosTablero.remove(coordenadaInicio);
                esValido = false;
            }
        }
        //setCoordenadaCasilla(coordenadaInicio, posicion);
        return esValido;
    }

    @Override
    public void escribeEnCasilla(Point coordenadaInicio, int posicion) {

        Casilla[][] tableroAsterisco = this.getCasillas();
        tableroAsterisco[coordenadaInicio.x][coordenadaInicio.y + posicion] = getPalabra().getCasillas().get(posicion);
        setCoordenadaCasilla(coordenadaInicio, posicion);
    }

    @Override
    public void setCoordenadaCasilla(Point coordenadaInicio, int posicion) {
        this.palabra.getCasilla(posicion).setFila(coordenadaInicio.x);
        this.palabra.getCasilla(posicion).setColumna(coordenadaInicio.y + posicion);
    }

    @Override
    public String toString() {
        return "horizontal";
    }

}
