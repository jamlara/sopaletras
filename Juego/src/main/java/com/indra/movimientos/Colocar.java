/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.movimientos;

import com.indra.juego.Casilla;
import java.awt.Point;
import java.util.List;
import java.util.Random;
import com.indra.juego.PalabraCasilla;
import com.indra.juego.Tablero;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author w10
 */
public abstract class Colocar {
    
    protected Tablero tablero;
    
    protected PalabraCasilla palabra;
    
    protected List<Point> puntosTablero = new ArrayList();
    
    public Colocar(PalabraCasilla palabra){
        
        this.palabra = palabra;
    }
    
    public abstract boolean validarCasilla(Point coordenadaInicio, int posicion);
    
    public abstract void escribeEnCasilla(Point coordenadaInicio, int posicion);
    
    public boolean contruyePalabra(Tablero tablero){
        
        this.setTablero(tablero);
        
        int filas = tablero.getFilas();
        int columnas = tablero.getColumnas();
        this.generarPuntosTablero(filas, columnas);
        boolean esCasillaValida = false;
        Point coordenadaInicio = null;
        
        boolean esInvertida = new Random().nextBoolean();
        if(esInvertida) Collections.reverse(this.palabra.getCasillas());

        while (!esCasillaValida && puntosTablero.size() > 1) {
            esCasillaValida = true;
            coordenadaInicio = puntosTablero.get(new Random().nextInt(puntosTablero.size()));
            //System.out.println(this.palabra.toString() + ": " + coordenadaInicio.x + ", " + coordenadaInicio.y + " | " + this.toString());
            for (int i = 0; i < getPalabra().getTamaño() && esCasillaValida; i++) {
                esCasillaValida = validarCasilla(coordenadaInicio, i);
            }
        }

        if (esCasillaValida && !puntosTablero.isEmpty()) {
            this.palabra.setValida(true);
            //System.out.println(this.palabra.toString());
            for (int i = 0; i < getPalabra().getTamaño(); i++) {
                escribeEnCasilla(coordenadaInicio , i);
            }
        }
        if(esInvertida) Collections.reverse(this.palabra.getCasillas());
        return esCasillaValida;
    }
    
    public abstract void generarPuntosTablero(int filas, int columnas);
    
    public abstract void setCoordenadaCasilla(Point coordenadas, int posicion);
    
    public PalabraCasilla getPalabra() {
        return palabra;
    }

    public void setPalabra(PalabraCasilla palabra) {
        this.palabra = palabra;
    }

    public boolean esPalabraInversa(){
        return new Random().nextBoolean();
    }

    public List<Point> getPuntosTablero() {
        return puntosTablero;
    }

    public void setPuntosTablero(List<Point> puntosTablero) {
        this.puntosTablero = puntosTablero;
    }

        public Tablero getTablero() {
        return tablero;
    }

    public void setTablero(Tablero tablero) {
        this.tablero = tablero;
    }
    
    public Casilla[][] getCasillas(){
        return this.getTablero().getCasillas();
    }
    
}