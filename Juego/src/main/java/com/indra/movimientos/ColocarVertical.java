/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.movimientos;

import com.indra.juego.Casilla;
import com.indra.juego.PalabraCasilla;
import java.awt.Point;
import java.security.SecureRandom;
import java.util.Random;

/**
 *
 * @author w10
 */
public class ColocarVertical extends Colocar {

    public ColocarVertical(PalabraCasilla palabra) {
        super(palabra);
    }

    @Override
    public void generarPuntosTablero(int filas, int columnas) {
        for (int i = 0; i <= filas - getPalabra().getTamaño(); i++) {
            for (int j = 0; j < columnas; j++) {
                puntosTablero.add(new Point(i, j));
            }
        }
    }

    @Override
    public boolean validarCasilla(Point coordenadaInicio, int posicion) {

        Casilla[][] tableroAsterisco = this.getCasillas();

        boolean esValido = true;

        if (tableroAsterisco[coordenadaInicio.x + posicion][coordenadaInicio.y].getCaracter() != '*') {
            if (tableroAsterisco[coordenadaInicio.x + posicion][coordenadaInicio.y].getCaracter() != getPalabra().getCasillas().get(posicion).getCaracter()) {
                puntosTablero.remove(coordenadaInicio);
                esValido = false;
            }
        }
        //setCoordenadaCasilla(coordenadaInicio, posicion);
        return esValido;
    }

    @Override
    public void escribeEnCasilla(Point coordenadaInicio, int posicion) {

        Casilla[][] tableroAsterisco = this.getCasillas();
        tableroAsterisco[coordenadaInicio.x + posicion][coordenadaInicio.y] = getPalabra().getCasillas().get(posicion);
        setCoordenadaCasilla(coordenadaInicio, posicion);
    }

    @Override
    public void setCoordenadaCasilla(Point coordenadaInicio, int posicion) {
        this.palabra.getCasilla(posicion).setFila(coordenadaInicio.x + posicion);
        this.palabra.getCasilla(posicion).setColumna(coordenadaInicio.y);
    }

    @Override
    public String toString() {
        return "vertical";
    }

}
