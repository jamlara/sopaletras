/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author w10
 */
public class TableroSopaLetras extends Tablero {
    
    List<PalabraCasilla> palabrasValidas = new ArrayList();
    List<PalabraCasilla> palabrasExcluidas = new ArrayList();

    public TableroSopaLetras(int filas, int columnas) {
        super(filas, columnas);
        //System.out.println("Creacion de un objeto TableroSopaLetras");
        inicializarCasillasTablero();
    }

    private void inicializarCasillasTablero() {
        for (int i = 0; i < this.getFilas(); i++) {
            for (int j = 0; j < this.getColumnas(); j++) {
                this.getCasillas()[i][j] = new Casilla(i, j, '*');
            }
        }
    }

    @Override
    public void llenarTablero(List<List<Casilla>> palabrasSolucion) {
        List<PalabraCasilla> palabras = new ArrayList();
        palabras = this.convertirListaCasillaAListaPalabra(palabrasSolucion);
        this.depurarPalabras(palabras);
        this.colocarPalabras(palabrasValidas);
        this.depurarPalabrasValidas();
    }

    private void colocarPalabras(List<PalabraCasilla> palabrasCasilla) {
        for (PalabraCasilla palabraCasilla : palabrasCasilla) {
            palabraCasilla.colocar(this);
        }
    }
    
    private void depurarPalabras(List<PalabraCasilla> palabras){
        for (int i = 0; i < palabras.size(); i++) { 
            if (palabras.get(i).getCasillas().size() > this.getColumnas() && 
                    palabras.get(i).getCasillas().size() > this.getFilas()) {
                palabrasExcluidas.add(palabras.get(i));
            } else {
                palabrasValidas.add(palabras.get(i));
            }
        }
    }
    
    private List<PalabraCasilla> convertirListaCasillaAListaPalabra(List<List<Casilla>> palabrasSolucion){
        List<PalabraCasilla> palabraCasilla = new ArrayList();
        for (List<Casilla> listaCasillas : palabrasSolucion) {
            palabraCasilla.add(new PalabraCasilla(listaCasillas, this.getFilas(), this.getColumnas()));
        }
        return palabraCasilla;
    }
    
    private boolean validarArea(){ //Ya no es necesario el método porque vamos a excluir las palabras que no entren en la sopa de letras
        int areaTablero = this.getFilas() * this.getColumnas();
        int totalCaracteres = 0;
        boolean hayEspacio = true;
        
        for (int i = 0; i < palabrasValidas.size(); i++) {
            totalCaracteres += palabrasValidas.get(i).getCasillas().size();
        }

        if (areaTablero < totalCaracteres) {
            System.out.println("Demasiadas palabras");
            hayEspacio = false;
        }
        return hayEspacio;
    }
    
    private void depurarPalabrasValidas(){
        for(int i = 0; i < palabrasValidas.size(); i++){
            if(!palabrasValidas.get(i).isValida()){
                //System.out.println("Se remueve " + palabrasValidas.get(i).toString() +  " ya que no se pudo incluir en la sopa de letras");
                palabrasExcluidas.add(palabrasValidas.get(i));
                palabrasValidas.remove(i);
            }
        }
    }
    
    public void mostrarPalabrasAEncontrarSolucion(){
        System.out.println("\nSoluciones: ");
        for (PalabraCasilla palabra : palabrasValidas) {
            System.out.println("\t" + palabra.toString() + ": \n\t"
                    + "Inicio: (" + palabra.getCasillas().get(0).getFila() + ", " + palabra.getCasillas().get(0).getColumna() + ")\t"
                    + "Fin: (" + palabra.getCasillas().get(palabra.getTamaño()-1).getFila() + ", " + palabra.getCasillas().get(palabra.getTamaño()-1).getColumna() + ")\n");
        }
    }
    
    public void mostrarPalabrasAEncontrar(){
        System.out.println("\nEncuentra estas palabras: ");
        for (PalabraCasilla palabra : palabrasValidas) {
            System.out.println("\t" + palabra.toString());
        }
    }
    
    public void mostrarPalabrasExcluidas(){
        if (!palabrasExcluidas.isEmpty()) {
            System.out.println("\nPalabras Excluidas: ");
            for (PalabraCasilla palabra : palabrasExcluidas) {
                System.out.println("\t" + "\u001B[31m" + palabra.toString() + "\u001B[0m");
            }
        }
    }
    
    private char darLetraAleatoria() {
        Random r = new Random();
        String alphabet = "abcdefghijklmnñopqrstuvwxyz";
        return alphabet.charAt(r.nextInt(alphabet.length()));
    }

    @Override
    public boolean validarTamaño() {
        return false;
    }

    @Override
    public void mostrarTablero() {
        System.out.println("\n\n------------------");
        System.out.println("| SOPA DE LETRAS |");
        for(int i = 0; i < (this.getColumnas() + 1)*3 + 1; i++){
            System.out.print("-");
        }
        System.out.println("");
        System.out.print("    ");

        for (int i = 0; i < this.getColumnas(); i++) {
            System.out.print(String.format(" %2d", i));
        }
        System.out.println("\n");
        for (int i = 0; i < this.getFilas(); i++) {
            System.out.print(String.format("%2d  ", i));
            for (int j = 0; j < this.getColumnas(); j++) {
                if (this.getCasillas()[i][j].getCaracter() == '*') {
                    System.out.print(String.format(" %2c", darLetraAleatoria()));
                } else {
                    System.out.print(String.format(" %2c", this.getCasillas()[i][j].getCaracter()));
                }
            }
            System.out.println("");
        }
        for(int i = 0; i < (this.getColumnas() + 1)*3 + 1; i++){
            System.out.print("-");
        }
        System.out.println("");

    }
    
    @Override
    public void mostrarTableroSolucion() {
        System.out.println("\n\n--------------");
        System.out.println("|  SOLUCION  |");
        for(int i = 0; i < (this.getColumnas() + 1)*3 + 1; i++){
            System.out.print("-");
        }
        System.out.println("");
        System.out.print("    ");

        for (int i = 0; i < this.getColumnas(); i++) {
            System.out.print(String.format(" %2d", i));
        }
        System.out.println("\n");
        for (int i = 0; i < this.getFilas(); i++) {
            System.out.print(String.format("%2d  ", i));
            for (int j = 0; j < this.getColumnas(); j++) {
                System.out.print(String.format(" %2c", this.getCasillas()[i][j].getCaracter()));
            }
            System.out.println("");
        }
        for(int i = 0; i < (this.getColumnas() + 1)*3 + 1; i++){
            System.out.print("-");
        }
        System.out.println("");
    }
    
    public boolean listaPalabrasValidasVacia(){
        return palabrasValidas.isEmpty();
    }
}
