/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

import java.util.List;

/**
 *
 * @author w10
 */
public abstract class Tablero {

    private int filas;
    private int columnas;
    private Casilla[][] casillas;
    private int numeroPalabras;
    private List<List<Casilla>> soluciones;
    
    public Tablero(int filas, int columnas){
        this.filas = filas;
        this.columnas = columnas;
        casillas = new Casilla[filas][columnas];
    }
    
   
    
    protected abstract void llenarTablero(List<List<Casilla>> listaCasillas);
    
    protected abstract boolean validarTamaño();
    
    public abstract void mostrarTablero();
    
    public abstract void mostrarTableroSolucion();
    
    public int getFilas() {
        return filas;
    }

    protected void setFilas(int filas) {
        this.filas = filas;
    }

    public int getColumnas() {
        return columnas;
    }

    protected void setColumnas(int columnas) {
        this.columnas = columnas;
    }

    public Casilla[][] getCasillas() {
        return casillas;
    }

    protected void setCasillas(Casilla[][] casillas) {
        this.casillas = casillas;
    }

    public int getNumeroPalabras() {
        return numeroPalabras;
    }

    protected void setNumeroPalabras(int numeroPalabras) {
        this.numeroPalabras = numeroPalabras;
    }
}
