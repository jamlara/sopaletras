/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

import com.indra.movimientos.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author w10
 */
public class PalabraCasilla {

    private List<Casilla> casillas;

    private List<Colocar> manerasDeColocar;

    private boolean valida = false;
    
    public PalabraCasilla(List<Casilla> casillas, int filas, int columnas) {

        this.casillas = casillas;
        this.manerasDeColocar = new ArrayList<>();
        iniciarGradosLibertad(filas, columnas);
    }

    private void iniciarGradosLibertad(int filas, int columnas) {

        int tamañoPalabra = casillas.size();

        if (tamañoPalabra <= columnas) {
            manerasDeColocar.add(new ColocarHorizontal(this));
        }
        if (tamañoPalabra <= filas) {
            manerasDeColocar.add(new ColocarVertical(this));
        }
        if (tamañoPalabra <= columnas && tamañoPalabra <= filas) {
            manerasDeColocar.add(new ColocarDiagonalArriba(this));
            manerasDeColocar.add(new ColocarDiagonalAbajo(this));
        }

    }

    public void colocar(Tablero tablero) {

        //System.out.println(manerasDeColocar.size());
        int opcion;// = new Random().nextInt(manerasDeColocar.size());
        boolean seColoco = false;
        do {
            opcion = new Random().nextInt(manerasDeColocar.size());
            seColoco = manerasDeColocar.get(opcion).contruyePalabra(tablero);
            //System.out.println(this.toString() + " " + manerasDeColocar.get(opcion).toString());
            if (!manerasDeColocar.isEmpty()) {
                //System.out.println("CAMBIE MI ESTADO" + manerasDeColocar.get(opcion).toString());
                manerasDeColocar.remove(opcion);
            }
        } while (!seColoco && !manerasDeColocar.isEmpty());
        
//        if(!seColoco && !manerasDeColocar.isEmpty()){
//            System.out.println("Excluida " + this.toString());
//        }else{
//            System.out.println("Incluida " + this.toString());
//        }     
        //System.out.println(manerasDeColocar.isEmpty() ? "No hay maneras de colocar" : "si pude :v ");
    }

    public boolean esPalabraValida() {
        //       return gradoHorizontal || gradoVertical;
        return true;
    }

    public int getTamaño() {
        return casillas.size();
    }

    public List<Casilla> getCasillas() {
        return casillas;
    }

    public Casilla getCasilla(int i) {
        return this.casillas.get(i);
    }

    public boolean isValida() {
        return valida;
    }

    public void setValida(boolean valida) {
        this.valida = valida;
    }

    @Override
    public String toString() {
        String palabra = "";
        for (int i = 0; i < casillas.size(); i++) {
            palabra += casillas.get(i).getCaracter();
        }
        return palabra;
    }

}
