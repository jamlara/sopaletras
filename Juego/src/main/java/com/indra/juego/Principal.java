/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

/**
 *
 * @author w10
 */
public class Principal {
    
    public static void main(String args[]){
        
        Lector.leerDimensiones();
        JuegoSopaLetras juego = new JuegoSopaLetras( new TableroSopaLetras(Lector.getFilas(), Lector.getColumnas()) );
        juego.iniciarJuego();
        
    }
}
