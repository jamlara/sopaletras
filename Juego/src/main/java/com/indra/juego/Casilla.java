/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

/**
 *
 * @author w10
 */
public class Casilla {
    
    private int fila;
    private int columna;
    private boolean esParteDeSolucion;
    private char caracter;

    public Casilla(int fila, int columna, char caracter) {
        this.fila = fila;
        this.columna = columna;
        this.caracter = caracter;
    }
    
    public Casilla(char caracter){
        this.caracter = caracter;
    }
    
    public Casilla(char caracter, boolean esParteDeSolucion){
        this.caracter = caracter;
        this.esParteDeSolucion = esParteDeSolucion;
    }
    
    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public boolean isEsParteDeSolucion() {
        return esParteDeSolucion;
    }

    public void setEsParteDeSolucion(boolean esParteDeSolucion) {
        this.esParteDeSolucion = esParteDeSolucion;
    }

    public char getCaracter() {
        return caracter;
    }

    public void setCaracter(char caracter) {
        this.caracter = caracter;
    }
    
}
