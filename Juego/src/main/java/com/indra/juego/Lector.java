/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author w10
 */
public class Lector {

    private List<String> diccionario;
    private final List<List<Casilla>> palabrasSolucion;
    private static int FILAS = 5;
    private static int COLUMNAS = 5;

    public Lector() {
        palabrasSolucion = new ArrayList<>();
        diccionario = new ArrayList<>();
        leeArchivo();
    }

    private void leeArchivo() { // Lectura del diccionario y almacenamiento en una lista de palabras
        FileReader fr = null;
        BufferedReader br = null;
        String diccionario = "src/main/java/resources/diccionario.txt";
        String linea;
        try {
            fr = new FileReader(diccionario);
            br = new BufferedReader(fr);
            while ((linea = br.readLine()) != null) {
                String UTF8Str = new String(linea.getBytes(), "UTF-8");
                this.diccionario.add(UTF8Str);
            }
        } catch (IOException ioe) {
            System.out.println("Error al abrir el archivo: " + ioe);
            this.diccionario = null;
        }
    }

    public void leerPalabrasAleatorias() {
        boolean entradaCorrecta = false;
        int numeroPalabrasAleatorias = 0;
        do {
            System.out.println("Cuantas palabras aleatorias quieres: ");
            try {
                Scanner escaner = new Scanner(System.in);
                numeroPalabrasAleatorias = escaner.nextInt(); // numero palabras aleatorias
                entradaCorrecta = true;
            } catch (InputMismatchException Ex) {
                System.out.println("Debe ingresar un número");
            } 
        } while (!entradaCorrecta || numeroPalabrasAleatorias < 0);
        generaPalabrasAleatoriasCasilla(numeroPalabrasAleatorias);
    }

    public void leerPalabrasCustom() {
        boolean entradaCorrecta = false;
        Scanner escaner;
        int numeroPalabrasCustom = 0;
        do {
            System.out.println("Cuantas palabras personalizadas quieres: ");
            try {
                escaner = new Scanner(System.in);
                numeroPalabrasCustom = escaner.nextInt(); // numero palabras custom
                entradaCorrecta = true;
            } catch (InputMismatchException Ex) {
                System.out.println("Debe ingresar un número");
            }
        } while (!entradaCorrecta || numeroPalabrasCustom < 0);

        for (int i = 0; i < numeroPalabrasCustom; i++) {

            System.out.println("Introduzca la palabra " + Integer.toString(i + 1) + ": ");
            escaner = new Scanner(System.in);
            String palabraCustom = escaner.nextLine();
            this.palabrasSolucion.add(generaPalabraCasilla(palabraCustom.toLowerCase()));

        }
    }

    private List<Casilla> generaPalabraCasilla(String palabra) { // Convierte un string en una lista de casillas

        List<Casilla> casillas = new ArrayList<>();
        palabra = palabra.replace('ñ', '\001');
        palabra = Normalizer.normalize(palabra, Normalizer.Form.NFD);
        palabra = palabra.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        palabra = palabra.replace('\001', 'ñ');
        for (int i = 0; i < palabra.length(); i++) {
            casillas.add(new Casilla(palabra.charAt(i), true));
        }

        return casillas;
    }

    private String generaPalabraAleatoria() { // solo genera una palabra aleatoria del diccionario
        String palabraAleatoria;
        SecureRandom sr = new SecureRandom();
        int palabraAleatoriaDiccionario = sr.nextInt(diccionario.size());
        palabraAleatoria = diccionario.get(palabraAleatoriaDiccionario);
        diccionario.remove(palabraAleatoriaDiccionario); // validacion de obtener la misma palabra del diccionario
        return palabraAleatoria.toLowerCase();
    }

    private void generaPalabrasAleatoriasCasilla(int numPalabras) { //genera una lista de palabras aleatorias

        for (int i = 0; i < numPalabras; i++) {
            this.palabrasSolucion.add(generaPalabraCasilla(generaPalabraAleatoria()));
        }
    }

    public List<List<Casilla>> getPalabrasSolucion() {
        return palabrasSolucion;
    }

    public static void leerDimensiones() {
        String resultado;
        String[] valores;
        do {
            System.out.println("Escribe las dimensiones del tablero (NxN): ");
            Scanner escaner = new Scanner(System.in);
            resultado = escaner.findInLine("[0-9]+x[0-9]+");
        } while (resultado == null);
        valores = resultado.split("x");
        FILAS = Integer.parseInt(valores[0]);
        COLUMNAS = Integer.parseInt(valores[1]);
    }
    
    public static int getFilas(){
        return FILAS;
    }
    
    public static int getColumnas(){
        return COLUMNAS;
    }

}
