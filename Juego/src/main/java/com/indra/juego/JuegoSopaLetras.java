/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

/**
 *
 * @author w10
 */
public class JuegoSopaLetras extends Juego {

    public JuegoSopaLetras(Tablero tablero) {
        super(tablero);
    }

    @Override
    public void iniciarJuego() {

        Lector datos = new Lector();
        datos.leerPalabrasAleatorias();
        datos.leerPalabrasCustom();

        TableroSopaLetras tablero = (TableroSopaLetras) this.tablero;

        tablero.llenarTablero(datos.getPalabrasSolucion());
        if (!tablero.listaPalabrasValidasVacia()) {
            tablero.mostrarTablero();
            tablero.mostrarPalabrasAEncontrar();
            tablero.mostrarPalabrasExcluidas();
            tablero.mostrarTableroSolucion();
            tablero.mostrarPalabrasAEncontrarSolucion();
        }else{
            System.out.println("Ninguna palabra se pudo acomodar");
            tablero.mostrarPalabrasExcluidas();
        }
    }

}
