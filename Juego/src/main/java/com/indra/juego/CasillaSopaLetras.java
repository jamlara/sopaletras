/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.indra.juego;

/**
 *
 * @author w10
 */
public class CasillaSopaLetras extends Casilla{
    
    public CasillaSopaLetras(int fila, int columna, char caracter) {
        super(fila, columna, caracter);
    }
    
     public CasillaSopaLetras(char caracter){
        super(caracter);
    }
    
    public CasillaSopaLetras(char caracter, boolean esParteDeSolucion){
        super(caracter, esParteDeSolucion);
    }
    
}
